<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class delivery_status extends Model
{
    protected $fillable =['id','name','icon'];

    public function orders()
    {
        return $this->hasMany('App\orders');
    }
}
