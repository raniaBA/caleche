<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PolygoneController extends Controller
{
    public function showpolygone()
    {
        return view('pages.polygone');
    }
}
