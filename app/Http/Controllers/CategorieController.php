<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\produit;
use Illuminate\Support\Facades\DB;

class CategorieController extends Controller {


    /**
     * CategorieController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function showCategorie($id)
    {

//        $produits= DB::table('produits')->where('categorie_id','=',$id)->get();
        $produits = produit::where('categorie_id', $id)->get();

        return view('pages.categorie',compact('produits'));
    }

}