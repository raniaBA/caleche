<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\produit;
use Illuminate\Support\Facades\DB;

class DetailController extends Controller {


    public function showDetail($id)
    {
//        $detail= DB::table('produits')->where('id','=',$id)->get();
        $detail = produit::where('id', $id)->get();

        $produits = produit::where('categorie_id', $detail[0]->categorie_id)->take(5)->get();

        $det=$detail[0];
        return view('pages.detail',['det'=>$det,'produits'=>$produits]);
    }

}