<?php

namespace App\Http\Controllers;


use Cart;
use Illuminate\Support\Facades\Cookie;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mailer ;

use Session;


class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $currency = env('BACKEND_URL');
        view::share('backurl', $currency);
    }
    protected function sendMail($to ,  array $data = []) {

        Mail::to($to)
            ->send(new Mailer($data)) ;

    }


}
