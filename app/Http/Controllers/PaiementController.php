<?php namespace App\Http\Controllers;

use App\Adresses;
use App\categorie;
use App\Http\Controllers\Controller;

use App\payment_method;
use App\zone;
use Illuminate\Support\Facades\DB;
use Auth;
use Cart;
use Illuminate\Http\Request;
use Session;
class PaiementController extends Controller {


    /**
     * PaiementController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function showpaiement()
    {
        $zones=Adresses::all();
        $cartCollection = Cart::getContent();
        $num = $cartCollection->count();
        if(Auth::check()) {
            if( $num!=0){
                $polygones=zone::All();

                $paymet_method=payment_method::All();
                $poly = explode(',', $polygones[0]->polygone);
            $i = 0;
            foreach ($poly as $p) {
                $pos = array("(", ")");
                $new = str_replace($pos, "", $p);
                $poly[$i] = $new;
                $i++;
            }

            return view('pages.paiement',['poly'=>$poly,'paymet_method'=>$paymet_method,'zones'=>$zones]);
            }else{
                return redirect('/');
            }
        }else{
            return view('auth.login');

        }
    }
    public function addV(Request $request){

        $methodeLiv=$request->get('optradio');
        $selectPay=$request->get('optradio2');
//        $val = DB::table('payment_methods')->where('name', $selectPay)->first();
        $val = payment_method::where('name', $selectPay)->get();

        $pannier = Cart::getContent();
        $totalPrice=Cart::getTotal();
        if($methodeLiv=='livraison'){
             $totalPrice=$totalPrice+8.000;
        }


        DB::table('orders')->insert(
            ['delivery_date_time' => '2019-07-22 13:06:26', 'total_price' => $totalPrice,'delivery_status_id' => 1,'payment_id' => $val[0]->id,'client_id' => Auth::user()->id,'created_at'=>now(),'updated_at'=>now()]
        );

        $order_id=DB::table('orders')->latest('id')->first();

        foreach ($pannier as $p){

            DB::table('orders_produit')->insert(
                ['produit_id' =>$p->id ,'quantity'=>$p->quantity,'orders_id' =>$order_id->id,'created_at'=>now(),'updated_at'=>now()]
            );

            Cart::remove($p->id);
            $num = Cart::getContent()->count();
            Session::put('count', $num);
        }


        $data = [
            'name'=>'rania',
            'message'=>'Votre demande est en attente',
            'from'=>'caleche.test@gmail.com',
            'view'=>'to_client.accepted',
            'subject'=>'Acceptation d\'inscription Resotel ',
        ] ;

        $this->sendMail('raniabelarbi70@gmail.com',$data);

        return redirect('/')->with('alert', 'Updated!'); ;

    }

}