<?php namespace App\Http\Controllers;

use App\Adresses;
use App\Http\Controllers\Controller;
use App\payment_method;
use App\User;
use App\zone;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
class ProfileClientController extends Controller {


    /**
     * ProfileClientController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()
    {
//        $polygones= DB::table('zones')->get();
        $user=User::find(Auth::id());

        $zones=Adresses::all()->where('client_id',$user->id);
        $polygones=zone::All();

//        $paymet_method= DB::table('payment_methods')->get();

        $paymet_method=payment_method::All();

        $poly = explode(',', $polygones[0]->polygone);
        $i = 0;
        foreach ($poly as $p) {
            $pos = array("(", ")");
            $new = str_replace($pos, "", $p);
            $poly[$i] = $new;
            $i++;
        }
        return view('pages.profileClient',['poly'=>$poly,'paymet_method'=>$paymet_method,'zones'=>$zones]);
    }
    public function update(Request $request){

//        $user= DB::table('clients')->where('id','=',Auth::id())->first();
        $user=User::find(Auth::id());
        $user->name = $request->get('name');
        $user->prenom = $request->get('prenom');
        $user->phone = $request->get('phone');
        $user->gouvernorat = $request->get('gouvernorat');
        $user->save();
        return view('pages.profileClient');
    }
    public function storeAdress(Request $request){
        $user=User::find(Auth::id());
        $adress=$request->get('adresse');
        $msg=$request->get('msg');
        $lat=$request->get('lat');
        $lng=$request->get('lng');

        if($msg=='1') {
            $adress = new Adresses([
                'adresse' => $adress,
                'client_id' => $user->id,
                'lat' => $lat,
                'lng' => $lng
            ]);
            $adress->save();
            return redirect()->back();

        }else{
            return redirect()->back();

        }
    }

}