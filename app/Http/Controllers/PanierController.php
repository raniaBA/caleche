<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\produit;
use Cart;
//use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Session;
class PanierController extends Controller
{

    public function __construct()
    {
        parent::__construct();

    }


    public function showPanier()
    {
        $totalPrice=Cart::getTotal();

        return view('pages.panier', ['array' => Cart::getContent(),'totalPrice'=>$totalPrice]);
    }

    public function addQnt($id){

        Cart::update($id, array(
            'quantity' => 1, // so if the current product has a quantity of 4, another 2 will be added so this will result to 6
        ));


        return  redirect()->back()->withErrors(['Ajouter au pannier avec succée']);

    }

    public function decQnt($id){

        Cart::update($id, array(
            'quantity' => -1, // so if the current product has a quantity of 4, another 2 will be added so this will result to 6
        ));
        return  redirect()->back();

    }
    public function shopping($id)
    {
        $produit = produit::where('id', $id)->get();

        Cart::add(array(
            'id' => $produit[0]->id,
            'name' => $produit[0]->name,
            'price' => $produit[0]->price,
            'quantity' => 1,
            'attributes' => array()
        ));
        $cartCollection = Cart::getContent();

        $num = $cartCollection->count();
        $totalPrice=Cart::getTotal();

        Session::put('count', $num);

        return  redirect()->back()->withErrors(['Ajouter au pannier avec succée']);
//        return view('pages.panier', ['array' => Cart::getContent(),'totalPrice'=>$totalPrice]);

    }

    public function delete($id)
    {

        Cart::remove($id);
        $num = Cart::getContent()->count();

        Session::put('count', $num);
        $totalPrice=Cart::getTotal();
        return view('pages.panier', ['array' => Cart::getContent(),'totalPrice'=>$totalPrice]);
    }



}