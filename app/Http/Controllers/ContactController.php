<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function PostContact(Request $request){

        $request->validate([
            'nom'=>'required',
            'email'=>'required',
            'sujet'=>'required',
            'message'=>'required',
        ]);
        $message='vous avez reçu un nouveau message from '.$request->get('nom');


        $contact = new Contact([
            'nom' => $request->get('nom'),
            'email' => $request->get('email'),
            'message' => $request->get('message'),
            'sujet' => $request->get('sujet'),
            'vu'=>1
        ]);
        $contact->save();

        $data = [
            'name'=>$request->get('nom'),
            'message'=>$message,
            'from'=>$request->get('email'),
            'view'=>'to_client.contact',
            'subject'=>'un nouveau message',
        ] ;

        $this->sendMail('caleche.test@gmail.com',$data);

        return redirect()->back()->with('message', 'IT WORKS!');

    }
}
