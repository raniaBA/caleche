<?php namespace App\Http\Controllers;

use App\categorie;
use App\Http\Controllers\Controller;
use App\produit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function showIndex()
    {

        $parent= DB::table('categories')
            ->select('parent')
            ->groupBy('parent')->take(5)
            ->get();

        $categorie=new categorie();

        $colors = ['#F3F3F1', '#DFE9F4', '#E5E5E2', '#F4F4E3', '#F4E6DC', '#EEFAFC'];
        return view('pages.index', ['colors' => $colors, 'parent' => $parent,'categorie'=>$categorie]);
    }




}