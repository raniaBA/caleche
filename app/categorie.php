<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categorie extends Model
{
    protected $fillable = [
        'parent' ,'icon', 'name',
    ];

    public function getCateg($parent){
        $liste=categorie::where('parent',$parent)->take(5)->get();
        return $liste;
    }

    public function produits()
    {
        return $this->hasMany('App\produit');
    }
//    public function getImageAttribute()
//    {
//        return $this->icon;
//    }
}
