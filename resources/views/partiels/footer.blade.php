{{--
<html>
<head>
    <link href="{{ URL::asset('css/footer.css')}}" rel="stylesheet" type="text/css" >
    --}}{{--google maps--}}{{--

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://maps.google.com/maps/api/js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>
    --}}{{--google maps--}}{{--
</head>
</html>
<!-- Contact Section -->
<body>--}}

<div id="contact" class="text-center">
    <div class="container text-center">
        <div class="col-md-3">


            <h3>Telephone</h3>
            <div class="contact-item">
                <p>Please call</p>
                <p>71890328</p>
            </div>


            <h3>Address</h3>
            <div class="contact-item">
                <p>121 avenue de la liberté,</p>
                <p>1002 Tunis Belvedére</p>
            </div>


            <h3>Email</h3>
            <div class="contact-item">
                <p>contact.lacaleche@gmail.com</p>

            </div>

        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="section-title text-center">
                    <h3>Send us a message</h3>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <form name="sentMessage" id="contactForm"  method="post" action="{{ URL('/contact') }}">
                            @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" id="nom" name="nom" class="form-control" placeholder="Name" required="required">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required="required">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" id="sujet" name="sujet" class="form-control" placeholder="Sujet" required="required">
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message"  class="form-control" rows="4" placeholder="Message" required></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                        <div id="success"></div>
                        <button type="submit" class="btn btn-custom btn-lg">Send Message</button>
                    </form>

                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <iframe id="mymap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2012.101364934507!2d10.176676114780319!3d36.81823870249881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12fd348777b9d70d%3A0x860b3c6b08b8bbf6!2sLa+Cal%C3%A8che!5e0!3m2!1sfr!2stn!4v1560951701303!5m2!1sfr!2stn"  frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
