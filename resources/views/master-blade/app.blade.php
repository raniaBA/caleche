<html>
<head>
    <title>
        La caléche - @yield("title")
    </title>
    @section("topCss")
        <link href="{{ URL::asset("img/favicon.ico") }}"  rel="shortcut icon"  type="image/x-icon">
        <link href="{{ URL::asset("img/apple-touch-icon.png")}}"  rel="apple-touch-icon" >
        <link href="{{ URL::asset("img/apple-touch-icon-72x72.png")}}"  rel="apple-touch-icon" sizes="72x72" >
        <link href="{{ URL::asset("img/apple-touch-icon-114x114.png")}}"  rel="apple-touch-icon" sizes="114x114" >

        <!-- Bootstrap -->
        <link href=" {{ URL::asset("css/bootstrap.css")}}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset("fonts/font-awesome/css/font-awesome.css")}}"  rel="stylesheet" type="text/css" >

        <!-- Stylesheet
            ================================================== -->
        <link href="{{ URL::asset("css/style.css")}}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset("css/navbar.css")}}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset("css/footer.css")}}" rel="stylesheet" type="text/css" >

        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rochester" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    @show
    @section("topjs")
          <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    @show
</head>
<body id="page-top" class="@yield('bodyStyle')" data-spy="scroll" data-target=".navbar-fixed-top">
<div class="wrapper">
    @section("header")
    @show

    @yield("content")


    @section("footer")

            @include("partiels.footer")

    @show

</div>
@section("footerJs")
    <script src="{{ URL::asset("js/jquery.1.11.1.js")}}" type="text/javascript"></script>
    <script src="{{ URL::asset("js/bootstrap.js")}}" type="text/javascript" ></script>
    <script src="{{ URL::asset("js/SmoothScroll.js")}}" type="text/javascript" ></script>
    <script src="{{ URL::asset("js/jqBootstrapValidation.js")}}" type="text/javascript" ></script>
    {{--<script src="{{ URL::asset("js/contact_me.js")}}" type="text/javascript" src=""></script>--}}
    <script src="{{ URL::asset("js/main.js")}}" type="text/javascript" ></script>
    <script src="{{ URL::asset("js/js.js")}}" type="text/javascript" ></script>
@show
</body>
</html>