{{--@extends('master-blade/app')--}}
@extends('layouts.app')
@section('title','index')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
<!-- Navigation-->
@section('content')
    @include("partiels.navbar")


    <div style="margin-top: 10pc" class="container">
        <div class="row justify-content-center">


            <div style="margin: 2pc">
                <!-- CONNEXION -->


                <div style="margin: 2pc">
                <center>
                <form name="sentMessage"  novalidate id="sign_up" method="POST" action="{{ route('register') }}" >
                @csrf
                {{--<div class="row">--}}
                {{--<h3><strong>Login </strong></h3>--}}

                {{--<div class="col-md-12">--}}
                {{--<div class="form-group">--}}
                {{--<label>EMAIL</label>--}}
                {{--<input type="email" id="email" class="form-control"--}}
                {{--required="required">--}}
                {{--<p class="help-block text-danger"></p>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-md-12">--}}
                {{--<div class="form-group">--}}
                {{--<label>MOT DE PASSE</label>--}}
                {{--<input type="password" id="password" class="form-control"--}}
                {{--required="required">--}}
                {{--<p class="help-block text-danger"></p>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<button class="btnPr btn log" data-toggle="tab"--}}
                {{-->connecter--}}
                {{--</button>--}}

                {{--</div>--}}




                <div class="row">
                <h3><strong>Inscription</strong></h3>
                    <hr>
                <div class="col-md-12">
                <div class="form-group">
                <label style="color: #999999">NOM</label>
                {{--<input type="text" id="nom" class="form-control"--}}
                {{--required="required">--}}
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name Surname">
                @error('name')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="help-block text-danger"></p>
                </div>
                </div>

                <div class="col-md-12">
                <div class="form-group">
                <label style="color: #999999">PRENOM</label>
                {{--<input type="text" id="prenom" class="form-control"--}}
                {{--required="required">--}}

                <input id="prenom" type="text" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus placeholder="lastName ">
                @error('prenom')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="help-block text-danger"></p>
                </div>
                </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="color: #999999">GOUVERNORAT</label>
                            {{--<input type="text" id="prenom" class="form-control"--}}
                            {{--required="required">--}}

                            <input id="gouvernorat" type="text" class="form-control @error('gouvernorat') is-invalid @enderror" name="gouvernorat" value="{{ old('gouvernorat') }}" required autocomplete="gouvernorat" autofocus placeholder="gouvernorat ">
                            @error('gouvernorat')
                            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                <div class="col-md-12">
                <div class="form-group">
                <label style="color: #999999">TELEPHONE</label>
                {{--<input type="text" id="telephone" class="form-control"--}}
                {{--required="required">--}}
                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus placeholder="Phone ">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="help-block text-danger"></p>
                </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                <label>EMAIL</label>
                {{--<input type="email" id="email" class="form-control"--}}
                {{--required="required">--}}
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address">

                @error('email')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="help-block text-danger"></p>
                </div>
                </div>

                <div class="col-md-12">
                <div class="form-group">
                <label style="color: #999999">MOT DE PASSE</label>
                {{--<input type="password" id="password" class="form-control"--}}
                {{--required="required">--}}
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
                <p class="help-block text-danger"></p>
                </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                <label style="color: #999999">CONFIRMER LE MOT DE PASSE</label>
                {{--<input type="password" id="confirm_password" class="form-control"--}}
                {{--required="required">--}}
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">

                <p class="help-block text-danger"></p>
                </div>
                </div>
                <button type="submit" class="btnPr btn log" data-toggle="tab"
                >{{ __('Register') }}
                </button>

                </div>
                </form>
                </center>

                </div>

                <!-- END CONNEXION -->

            </div>


        </div>
    </div>


@endsection



@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent
@endsection

