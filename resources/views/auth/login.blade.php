{{--@extends('master-blade/app')--}}
@extends('layouts.app')
@section('title','index')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
<!-- Navigation-->
@section('content')
    @include("partiels.navbar")


    <div style="margin-top: 10pc" class="container">
        <div class="row justify-content-center">


            <div style="margin: 2pc">
                <!-- CONNEXION -->


                <div style="margin: 2pc">
                    <center>
                        <form name="sentMessage"  novalidate id="sign_in" method="POST" action="{{ route('login') }}" >
                            @csrf
                            <div class="row">
                            <h3><strong>Login </strong></h3>
                                <hr>

                            <div class="col-md-12">
                            <div class="form-group">
                            <label>EMAIL</label>
                            {{--<input type="email" id="email" class="form-control"--}}
                            {{--required="required">--}}

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                            <p class="help-block text-danger"></p>
                            </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                            <label>MOT DE PASSE</label>
                            {{--<input type="password" id="password" class="form-control"--}}
                            {{--required="required">--}}

                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                            <p class="help-block text-danger"></p>
                            </div>
                            </div>

                            <button class="btnPr btn log" data-toggle="tab"
                            >connecter
                            </button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif

                            </div>





                        </form>
                    </center>

                </div>

                <!-- END CONNEXION -->

            </div>


        </div>
    </div>


@endsection



@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent
@endsection

