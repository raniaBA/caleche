{{--@extends('master-blade/app')--}}
@extends('layouts.app')
@section('title','index')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
<!-- Navigation-->
@section('content')
    @include("partiels.navbar")


    <div style="margin-top: 10pc" class="container">
        <div class="row justify-content-center">


            <div style="margin: 2pc">
                <!-- CONNEXION -->


                <div style="margin: 2pc">
                    <center>
                        <form name="sentMessage"  novalidate id="sign_in" method="POST" action="{{ route('password.email') }}" >
                            @csrf
                            <div class="row">
                                <h3><strong>Reset Password </strong></h3>

                                <hr>
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>EMAIL</label>
                                        {{--<input type="email" id="email" class="form-control"--}}
                                        {{--required="required">--}}

                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button style="width: 12pc;" type="submit" class=" btnPr btn log" data-toggle="tab">
                                            {{ __('Send Password Reset Link') }}
                                        </button>
                                    </div>
                                </div>


                            </div>





                        </form>
                    </center>

                </div>

                <!-- END CONNEXION -->

            </div>


        </div>
    </div>


@endsection



@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent
@endsection

