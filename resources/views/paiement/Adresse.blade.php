
    <div class="row">


        <div style="margin: 2pc">
            <h3>Ajout d'adresse</h3>
            <form name="sentMessage" id="contactForm" novalidate>
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label style="color: #999999">ADRESSE</label>
                            <input type="text" id="adresse" class="form-control"
                                   required="required">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label style="color: #999999">GOUVERNORAT</label>
                        <input type="text" id="gouvernorat" class="form-control"
                               required="required">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="col-md-6">
                        <label style="color: #999999">VILLE</label>
                        <input type="text" id="ville" class="form-control" required="required">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="color: #999999">CODE P0OSTALE</label>
                            <input type="text" id="postal" class="form-control" required="required">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                </div>
            </form>

            <div class="row">
                <h3 style="margin: 2pc">Information sur la livraison</h3>

                <div class="col-md-6">
                    <div style="border: 1px solid lightgray;padding: 2pc">
                        <p>Jamy larson</p>
                        <p>121 avenue de la liberté</p>
                        <p>1002 Tunis Belvedére</p>
                        <p>HX1 5QN</p>
                        <p>tEL/ 71 890 328</p>
                    </div>
                </div>
                <div class="col-md-6" style=" overflow: hidden;">

                    <iframe style="width: 100%; height: 13.7pc;"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d102239.5835557102!2d10.073237303930252!3d36.79486241705152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12fd337f5e7ef543%3A0xd671924e714a0275!2sTunis!5e0!3m2!1sfr!2stn!4v1560854666838!5m2!1sfr!2stn"
                            width="600" height="450" frameborder="0" style="border:0"
                            allowfullscreen></iframe>

                </div>

            </div>

        </div>


    </div>

    <button class="btnPr btn continue suiv" data-toggle="tab">SUIVANT
    </button>
    <button class="btnPr btn back suiv " data-toggle="tab">
        RETOUR
    </button>
