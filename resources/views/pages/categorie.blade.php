@extends('master-blade/app')
@section('title','detail')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection


<!-- Navigation-->
@section('content')
    @include("partiels.navbar")
    <div class="container">
        <div class="row top">
            <div class="col-md-5">
                <div class="center">
                    <h1>PATISSERIE</h1>
                    <h5>Choisisez vos préférés maintenant</h5>


                    <button type="button" class="btnPr btn btn-lg">
                        Commander
                    </button>
                </div>

            </div>
            <div class="col-md-7">
                <img class="img-default img-responsive" src=" {{ URL::asset('img/gallery/04.jpg')}}" alt="">
            </div>


        </div>

        <div  class="list row">
            <div  class="leftSide col-md-3">
                <h4>Kitchen</h4>
                <div>
                    <input type="checkbox">

                    <span>Mille Fruilles(6)</span>

                </div>
                <div>
                    <input  type="checkbox">

                    <span>Tarte(13)</span>

                </div>
                <div class="check">
                    <input type="checkbox">

                    <span>Gateaux(16)</span>

                </div>
                <div>
                    <input type="checkbox">

                    <span>Charlotte(5)</span>

                </div>
                <div>
                    <input type="checkbox">

                    <span>Broken Rice(2)</span>

                </div>


            </div>
            <div class="col-md-9">
                <div class="row">

                    @foreach($produits as $p)
                    <div style="margin-top: 2pc" class="card col-xs-12 col-sm-4">
                        <div class="bord2 mets-item">

                            <div style="height: 8pc; width: 100%; text-align: center " >
                                <img style="display: block; margin-left: auto; margin-right: auto;height:100%" src="{{$backurl}}{{$p->image}}" class="img-responsive" alt="">
                            </div>
                            <div class="">
                                <h5>{{$p->name}}</h5>
                                <div>
                                <p>{{$p->description}} </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <a type="submit"  class="btn btn-dark" href="{{ route('detail',$p->id)}}">En savoir plus</a>

                                    </div>
                                    <div  class="col-md-5">
                                        <p>2.300</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>


                <button  type="button" class="btnF btn btn-lg">Voir plus
                </button>

            </div>


        </div>
    </div>



@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent
@endsection