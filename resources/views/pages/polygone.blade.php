@extends('master-blade/app')
@section('title','panier')
@section('topCss')
    @parent

@endsection

@section('topjs')


@endsection

@section('content')

    @include("partiels.navbar")
    <div class="container" style="padding-top: 4cm;padding-bottom: 4pc">
        <div id="map"></div>
    </div>
@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    <script>

        // This example creates a simple polygon representing the Bermuda Triangle.

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: {lat: 36.818338, lng: 10.178565},
                mapTypeId: 'terrain'
            });

            // Define the LatLng coordinates for the polygon's path.
            var Coords = [
                {lat: 36.815620, lng: 10.178530},
                {lat: 36.816277, lng: 10.183655},
                {lat: 36.817659, lng: 10.186155},
                {lat: 36.823133, lng: 10.185621},
                {lat: 36.818690, lng: 10.177191},
            ];

            // Construct the polygon.
            var zone = new google.maps.Polygon({
                paths: Coords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });

            // Marker.
            var marker = new google.maps.Marker({
                position:{lat: 36.818338, lng: 10.178565},
                animation: google.maps.Animation.DROP,
                map: map,
                label: {text: "La caléche!", color: "white"}
            });
            // Marker.
            var lastPosition = {lat: 36.818166, lng: 10.179091};
            var marker2 = new google.maps.Marker({
                position:{lat: 36.818166, lng: 10.179091},

                animation: google.maps.Animation.DROP,
                draggable: true,
                map: map,
                label: {text: "My place!", color: "white"}
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<strong>La Calèche</strong>, Avenue de La Liberté, Jeanne d'Arc"
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
            google.maps.event.addListener(marker2, 'dragend', function(e) {

                if( google.maps.geometry.poly.containsLocation(e.latLng, zone) ){
                    alert(marker2.getPosition());

                }
                else {
                    alert("Out of zone");
                    marker2.setPosition(lastPosition);
                }



            });
            zone.setMap(map);

        }



    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcJjEB7EAciEHHWbMe5Jk73AxNhlmSGew&callback=initMap">
    </script>
@endsection