@extends('master-blade/app')
@section('title','panier')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
@section('bodyStyle','grad')
@section('content')

    @include("partiels.navbar")

    <div class=" cont margTop container">
        <h3>Panier</h3>
        <hr>

        @foreach($array as $values)
            <div class="row">
                <div class="col-md-3"><img src="{{asset('img/mets/met3.png')}}" class="img-responsive" alt=""></div>
                <div class="paddingT col-md-3">

                    <strong> {{$values->name}}</strong> <br>



                    <small>patisserie</small>
                </div>
                <div style="padding-top: 4pc" class="col-md-2">{{$values->price}}DT</div>
                <div class="paddingT col-md-3">
                    <div class="input-group">
                          <span class="input-group-btn">

                               <button type="button" class="btn btn-default btn-number"valueId="{{$values->id}}"
                                       data-type="minus"
                                       data-field="quant[1]"  onclick="decValue({{$values->id}});">
                                  <span class="glyphicon glyphicon-minus"></span>
                              </button>
                          </span>
                        <input type="text" name="quant[1]" id="number_{{$values->id}}" class="form-control input-number"
                               value="{{$values->quantity}}">
                        <span class="input-group-btn">
                              <button  type="button" class="btn btn-default btn-number" valueId="{{$values->id}}" data-type="plus"
                                       data-field="quant[1]" onclick="incrementValue({{$values->id}});"   >
                                  <span class="glyphicon glyphicon-plus"></span>
                              </button>
                          </span>
                    </div>
                </div>




                <div style="padding-top: 4pc" class="col-md-1">
                    <a   href="{{ route('deleteCartItem',$values->id)}}" type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
            </div>
            <hr>
        @endforeach


        <div style="    float: right;">
            <h5>Montant toltal:{{$totalPrice}}DT</h5>
            <a  href="{{ route('paiement')}}" type="button" class="btnPr btn btn-lg" >
                Commander
            </a>
        </div>


        <script>
            function decValue(buttonId) {

                let id = buttonId ;

                let value = parseInt(document.getElementById('number_'+id).value, 10);

                if(value>1) {

                value = isNaN(value) ? 0 : value;
                value--;
                document.getElementById('number_'+id).value = value;
                    $.ajax({
                        url : "/decQnt/"+id ,
                        type : "get" ,
                        data : {

                        },
                    });
                }

            }
            function incrementValue(buttonId)
            {

                let id = buttonId ;

                let value = parseInt(document.getElementById('number_'+id).value, 10);

                if(value<10) {

                    value = isNaN(value) ? 0 : value;
                    value++;
                document.getElementById('number_'+id).value = value;
                    $.ajax({
                        url : "/addQnt/"+id ,
                        type : "get" ,
                        data : {

                        },
                    });
                }else{
                    alert('max 10')
                }


            }


        </script>



    </div>


@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent


@endsection