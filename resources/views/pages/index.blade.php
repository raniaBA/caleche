@extends('master-blade/app')
@section('title','index')
@section('topCss')
  @parent

@endsection

@section('topjs')
  @parent

@endsection


<!-- Navigation-->
@section('content')
  @include("partiels.navbar")

  <!-- Header -->
  <header  id="header">
    <div class="grad">
      <div class="overlay">
        <div class="container">
          <div class="row">
            <div class="intro-text">
              <img class="logo" src="{{ URL::asset('img/logo.png')}}" alt=""> <br>
              <button  type="button" class="btnl btn btn-lg" >Commander</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  @if(session()->has('message'))

    <script>
    alert('votre message est envoyé avec succé');
    </script>
  @endif
  <!-- mets Section -->
  <div id="mets" class="text-center">
    <div class="container">
      @foreach($parent as $p)

      <div class="row">
        <div class="section-title">
          <h2>NOS METS</h2>
        </div>

        <!-- carousel -->
        <div  class="carousel-size col-md-6 col-md-offset-3">
          <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">

            <div class="carousel-inner">


              <div  id="item" class="item active">
                <div  class="col-md-3 col-sm-6 ">
                  <div style="background-color:#F3F3F1;padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h4 style="letter-spacing: 8px;margin-bottom: 6pc">{{$categorie->getCateg($p->parent)[0]->name}}</h4></u>
                    <img  style="margin-bottom: 6pc" src="{{asset('img/mets/met1.png')}}" class="img-responsive" alt="">

                    <a href="{{ route('categorie',$categorie->getCateg($p->parent)[0]->id)}}" type="submit"  class=" btn button" >En savoir plus</a>

                  </div>
                </div>
              </div>

              @for ($i = 1; $i < count($categorie->getCateg($p->parent)); $i++)
                <div id="item" class="item">
                  <div  class="col-md-3 col-sm-6 ">
                    <div style="background-color:{{$colors[mt_rand(1, 5)]}};padding: 3pc 0pc 3pc 0pc;"  class="bord">
                      <u><h4 style="letter-spacing: 8px;margin-bottom: 6pc">{{$categorie->getCateg($p->parent)[$i]->name}}</h4></u>
                      <img style="margin-bottom: 6pc" src="{{asset('img/mets/met1.png')}}" class="img-responsive" alt="">

                      <a type="submit" class="btn button" href="{{ route('categorie',$categorie->getCateg($p->parent)[0]->id)}}">En savoir plus</a>

                    </div>
                  </div>
                </div>
              @endfor

            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
          </div>
        </div>

        <!-- carousel -->
      </div><br><br><br>
@endforeach
    </div>
  </div>
  <!-- About Section -->
  <div  id="about">
    <div class="container-fluid">
      <div style="padding: 3pc" class="row">
        <div  class="col-xs-12 col-md-6 about-img"> </div>
        <div class="col-xs-12 col-md-3 col-md-offset-1">
          <div class="about-text">
            <div class="section-title">
              <h2 class="titre">Notre Savoir-faire</h2>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam. Sed commodo nibh ante facilisis bibendum dolor feugiat at. Duis sed dapibus leo nec ornare diam commodo nibh.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam. Sed commodo nibh ante facilisis bibendum dolor feugiat at. Duis sed dapibus leo nec ornare.</p>
          </div>
        </div>
      </div>
    </div>
  </div>






  <!-- entremet Section -->
  <div id="entremet" class="text-center">
    <div class="container">
      <div class="row">
        <div class="section-title">
          <h2>NOS ENTREMETS</h2>
        </div>
        <!-- carousel -->
        <!-- carousel -->
        <div class="carousel-size col-md-6 col-md-offset-3">
          <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel2">
            <div class="carousel-inner">
              <div  class="item active">
                <div  class="col-md-3 col-sm-6 ">
                  <div style="background-color:{{$colors[mt_rand(1, 5)]}};padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h5 style="margin-bottom: 6pc">PATTES</h5></u>
                    <img style="margin-bottom: 6pc" src="{{asset('img/mets/met6.png')}}" class="img-responsive" alt="">

                    <button type="button" class="button" onclick="window.location='{{ url("/categorie") }}'">En savoir plus</button>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div style="background-color:{{$colors[mt_rand(1, 5)]}};padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h5 style="margin-bottom: 6pc">POULETS</h5></u>

                    <img style="margin-bottom: 6pc" src="{{asset('img/mets/met7.png')}}" class="img-responsive" alt="">

                    <button type="button" class="button" onclick="window.location='{{ url("/categorie") }}'">En savoir plus</button>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div style="padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h5 style="margin-bottom: 6pc">PIZZA</h5></u>

                    <img style="margin-bottom: 6pc" src="{{asset('img/mets/met8.png')}}" class="img-responsive" alt="">

                    <button type="button" class="button" onclick="window.location='{{ url("/categorie") }}'">En savoir plus</button>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div style="padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h5 style="margin-bottom: 6pc">POISSONS</h5></u>

                    <img style="margin-bottom: 6pc" src="{{asset('img/mets/met9.png')}}" class="img-responsive" alt="">

                    <button type="button" class="button" onclick="window.location='{{ url("/categorie") }}'">En savoir plus</button>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div style="padding: 3pc 0pc 3pc 0pc;"  class="bord">
                    <u><h5 style="margin-bottom: 6pc">POISSONS</h5></u>

                    <img style="margin-bottom: 6pc" src="{{asset('img/mets/met1.png')}}" class="img-responsive" alt="">

                    <button type="button" class="button" onclick="window.location='{{ url("/categorie") }}'">En savoir plus</button>
                  </div>
                </div>
              </div>


            </div>
            <a class="left carousel-control" href="#myCarousel2" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
            <a class="right carousel-control" href="#myCarousel2" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
          </div>
        </div>

        <!-- carousel -->
      </div>

    </div>
  </div>







@endsection



@section("footer")
  @parent

@endsection
@section("footerJs")
  @parent

  {{--<script>--}}
      {{--var colors = ['#F3F3F1', '#DFE9F4', '#E5E5E2','#F4F4E3', '#F4E6DC', '#EEFAFC'];--}}
      {{--var random_color = colors[Math.floor(Math.random() * colors.length)];--}}
      {{--alert(random_color);--}}
      {{--$('#item').css('color', random_color);--}}
  {{--</script>--}}
@endsection