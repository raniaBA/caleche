@extends('master-blade/app')
@section('title','panier')
@section('topCss')
    @parent

@endsection

@section('topjs')


@endsection

@section('content')

    @include("partiels.navbar")
    <div class="container" style="padding-top: 4cm;padding-bottom: 4pc">
        <div id="map"></div>
        <div id="info"></div>
    </div>
@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    <script>
        // This example requires the Drawing library. Include the libraries=drawing
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=drawing">
        var geocoder;

        var polygonArray = [];
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 36.818338, lng: 10.178565},
                zoom: 17
            });
            // Marker.
            var marker = new google.maps.Marker({
                position:{lat: 36.818338, lng: 10.178565},
                animation: google.maps.Animation.DROP,
                map: map,
                label: {text: "La caléche!", color: "white"}
            });
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [ 'polygon' ]
                },
                markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 1,
                    strokeWeight: 5,
                    clickable: false,
                    editable: true,
                    zIndex: 1
                }
            });

            drawingManager.setMap(map);
            google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
                document.getElementById('info').innerHTML += "polygon points:" + "<br>";
                for (var i = 0; i < polygon.getPath().getLength(); i++) {
                    document.getElementById('info').innerHTML += polygon.getPath().getAt(i).toUrlValue(6) + "<br>";
                    polygonArray.push({
                        lat: polygon.getPath().getAt(i).lat(),
                        lng: polygon.getPath().getAt(i).lng()
                    });
                }

            });



        }
    </script>
    {{--<script async defer--}}
    {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcJjEB7EAciEHHWbMe5Jk73AxNhlmSGew&libraries=drawing&callback=initMap"--}}
    {{--async defer></script>--}}

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcJjEB7EAciEHHWbMe5Jk73AxNhlmSGew&libraries=drawing&callback=initMap"
            async defer></script>



@endsection