@extends('master-blade/app')
@section('title','paiement')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
@section('bodyStyle','grad')

<!-- Navigation-->
@section('content')
    @include("partiels.navbar")
    <div class="container">
        <div class="margTop boxPos row">
            <div id="section" class="col-md-8">

                <ul id="tabs" class="nav nav-tabs">
                    <!-- <li class="active"><a data-toggle="tab" href="#home">Home</a></li>-->
                    <!--    <li class="active"><a data-toggle="tab" href="#menu1">01 INFOS CLIENTS</a></li> -->
                    {{--<li class="active"><a data-toggle="tab" href="#menu1">CONNEXION</a></li>--}}

                    <li class="active"><a data-toggle="tab" href="#menu2">LIVRAISON </a></li>

                    <li id="m3" style="pointer-events: none;" class="disabled"><a data-toggle="tab" href="#menu3">ADRESSE</a>
                    </li>

                    <li style="pointer-events: none;" class="disabled"><a data-toggle="tab" href="#menu4">SELECTION DU
                            PAIEMENT</a></li>
                    <li style="pointer-events: none;" class="disabled"><a data-toggle="tab" href="#menu5">PAIEMENT</a>
                    </li>

                </ul>
                <form action="{{ url('/paiementSave') }}" id="paiementf"  name="paiement" method="POST">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div class="tab-content">

                        <div id="menu2" class="tab-pane fade in active">


                            <div class="row">
                                <h3 style="margin: 2pc">Méthode de livraison</h3>
                                <div class="col-md-6">
                                    <div style="border: 1px solid lightgray;padding: 2pc">

                                        <input value="Retrait au magasin" type="radio" name="optradio"> <strong> Retrait au
                                            magasin </strong>
                                        <strong style="float: right"> Gratuit </strong>

                                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta
                                            sunt
                                            explicabo.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div style="border: 1px solid lightgray;padding: 2pc">
                                        <input value="livraison" type="radio" name="optradio"> <strong> Livraison</strong>
                                        <strong style="float: right"> 8.000 dt </strong>
                                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                            dicta
                                            sunt explicabo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h3 style="margin: 2pc">Information sur la livraison</h3>
                                <div class="col-md-6">
                                    <div style="border: 1px solid lightgray;padding: 2pc">
                                        <p>Jamy larson</p>
                                        <p>121 avenue de la liberté</p>
                                        <p>1002 Tunis Belvedére</p>
                                        <p>HX1 5QN</p>
                                        <p>tEL/ 71 890 328</p>
                                    </div>
                                </div>
                                <div class="col-md-6" style=" overflow: hidden;">

                                    <div style="height: 24%;" id="map" class="gmap"></div>

                                </div>

                            </div>
                            <button id="btn1" class="btnPr btn continue suiv" name="btn1" data-toggle="tab">SUIVANT
                            </button>

                        </div>


                        <div id="menu3" class="tab-pane fade">


                            <div class="row">


                                <div style="margin: 2pc">
                                    <h3>Ajout d'adresse</h3>
                                    <form name="sentMessage" id="contactForm" novalidate>
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label style="color: #999999">ADRESSE</label>
                                                    <select  class="form-control" id="select_id" >
                                                        <option value=""></option>
                                                        @foreach($zones as $z)
                                                        <option value="{{$z->adresse}}">{{$z->adresse}}</option>
                                                            @endforeach
                                                    </select>
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label style="color: #999999">GOUVERNORAT</label>
                                                <input type="text" id="g" class="form-control"
                                                       >
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="col-md-6">
                                                <label style="color: #999999">VILLE</label>
                                                <input type="text" id="v" class="form-control" >
                                                <p class="help-block text-danger"></p>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label style="color: #999999">CODE P0OSTALE</label>
                                                    <input type="text" id="p" class="form-control">
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>

                                        </div>
                                    </form>



                                </div>


                            </div>

                            <button id="btn" name="btn" class="btnPr btn continue suiv" data-toggle="tab">SUIVANT
                            </button>
                            <button class="btnPr btn back suiv " data-toggle="tab">
                                RETOUR
                            </button>


                        </div>

                        <div id="menu4" class="tab-pane fade">
                            {{--@include("paiement.selection")--}}

                            <h3>Sélection du paiement</h3>
                            @foreach($paymet_method as $pay)
                            <div class="row">
                                <div id="p1" style="border: 1px solid lightgray;margin: 1pc; width: 40pc;padding: 1pc">
                                    <input type="radio" name="optradio2" value="{{$pay->name}}"> <strong> {{$pay->name}} </strong>
                                    <div class="row">


                                        <div class="col-md-9">
                                            <small>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae
                                                vitae
                                                dicta
                                                sunt explicabo.
                                            </small>
                                        </div>
                                        <div class="col-md-3">
                                            <img src="https://img.icons8.com/color/30/000000/visa.png">
                                            <img src="https://img.icons8.com/color/30/000000/mastercard-credit-card.png">
                                            <img src="https://img.icons8.com/color/30/000000/amex.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            <button id="btn2" name="btn2" class="btnPr btn continue suiv " data-toggle="tab"
                                    onclick="$('#container').hide()">SUIVANT
                            </button>


                            <button class="btnPr btn back suiv" data-toggle="tab">
                                RETOUR
                            </button>


                        </div>
                        <div id="menu5" class="tab-pane fade">
                            {{--@include("paiement.paiement")--}}
                            <h3>Sélection du paiement</h3>
                            <div class="row">
                                <div style="border: 1px solid lightgray;margin: 1pc; width: 40pc;padding: 1pc">
                                    <div style="margin-top: 1pc;"><strong>Méthode livraison:</strong><p id="methode"></p> </div>
                                    <div style="margin-top: 1pc;"><strong>Sélection du paiement:</strong> <p id="selection"></p>
                                    </div>
                                    <div id="p2">
                                    <div style="margin-top: 1pc;"><strong>Adresse:</strong> <p id="adr"></p></div>
                                    <div style="margin-top: 1pc;"><strong>Gouvernorat: </strong> <p id="gouv"></p> </div>
                                    <div style="margin-top: 1pc;"><strong>Ville: </strong> <p id="ville"></p></div>
                                    <div style="margin-top: 1pc;"><strong>Code postal:</strong><p id="postal"></p> </div>
                                    </div>
                                    <div style="margin-top: 1pc;"><strong>Téléphone:</strong> {{ Auth::user()->phone }} </div>

                                </div>

                            </div>
                            <button id="btn2" name="dd" type="submit" class="btnPr btn"  style="width: 10pc;height: 2pc">
                                PAYEMENT
                            </button>
                        </div>

                    </div>
                </form>

            </div>
            <div id="container" class="col-md-4" style="background:#f1f1f1;margin-bottom: 2pc">
                <h3>Panier</h3>
                <hr style="border-top: 2px solid #d5d5d5;">
                <div class="row" style="margin-top: 2pc">
                    <div class="col-md-5">
                        <img style="width: 70%;float: right;" src="{{asset('img/c1.jpg')}}" class="img-responsive"
                             alt="">
                    </div>
                    <div class="paddingT col-md-7">
                        <strong>Mille feuilles aux fruits</strong>
                        <br>
                        <div style="padding-top: 1pc">2.200DT</div>
                    </div>

                </div>

                <div class="row" style="margin-top: 2pc">
                    <div class="col-md-5">
                        <img style="width: 70%;float: right;" src="{{asset('img/c1.jpg')}}" class="img-responsive"
                             alt="">
                    </div>
                    <div class="paddingT col-md-7">
                        <strong>Mille feuilles aux fruits</strong>
                        <br>
                        <div style="padding-top: 1pc">2.200DT</div>
                    </div>
                </div>

                <div class="row" style="margin-top: 2pc">
                    <div class="col-md-5">
                        <img style="width: 70%;float: right;" src="{{asset('img/c1.jpg')}}" class="img-responsive"
                             alt="">
                    </div>
                    <div class="paddingT col-md-7">
                        <strong>Mille feuilles aux fruits</strong>
                        <br>
                        <div style="padding-top: 1pc">2.200DT</div>
                    </div>


                </div>

                <div style="margin-top: 4pc" class="row">
                    <div class="col-md-6"><p style="color: #999999">Total</p></div>
                    <div class="col-md-6" style="float: right"><p style="color: #999999">15.800 DT</p></div>
                </div>
                <div class="row">
                    <div class="col-md-6"><p style="color: #999999">livraison</p></div>
                    <div class="col-md-6" style="float: right"><p style="color: #999999">8.000 DT</p></div>
                </div>

                <hr style="border-top: 2px solid #d5d5d5;">

                <div style="margin-top: 2pc;padding-bottom: 5pc" class="row">
                    <div class="col-md-6"><p style="color: #000000">Total</p></div>
                    <div class="col-md-6"><p style="color: #000000">23.800dt</p></div>
                </div>
            </div>
        </div>
    </div>

@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSkRhBbrsIG6XBn8Xt5qGBny6Anh6VXk8&callback=initMap">
    </script>
    <script>

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: {lat: 36.818338, lng: 10.178565},
                mapTypeId: 'terrain'
            });


            var Coords = [
                    @for($i=0;$i<count($poly); $i=$i+2)
                {
                    lat: parseFloat(['{{$poly[$i]}}']), lng: parseFloat(['{{$poly[$i+1]}}'])
                },
                @endfor
            ];
            console.log('cord', Coords);


            // Construct the polygon.
            var zone = new google.maps.Polygon({
                paths: Coords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });

            // Marker.
            var marker = new google.maps.Marker({
                position: {lat: 36.818338, lng: 10.178565},
                animation: google.maps.Animation.DROP,
                map: map,
                label: {text: "La caléche!", color: "white"}
            });
            // Marker.
            var lastPosition = {lat: 36.818166, lng: 10.179091};
            var marker2 = new google.maps.Marker({
                position: {lat: 36.818166, lng: 10.179091},

                animation: google.maps.Animation.DROP,
                draggable: true,
                map: map,
                label: {text: "My place!", color: "white"}
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<strong>La Calèche</strong>, Avenue de La Liberté, Jeanne d'Arc"
            });
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
            google.maps.event.addListener(marker2, 'dragend', function (e) {

                if (google.maps.geometry.poly.containsLocation(e.latLng, zone)) {
                    alert(marker2.getPosition());

                }
                else {
                    alert("Out of zone");
                    marker2.setPosition(lastPosition);
                }
            });
            zone.setMap(map);
        }
    </script>

    <script>
        $('.continue').click(function () {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        });
        $('.back').click(function () {
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#container').hide();
            document.getElementById("section").style.width = "100%";

            $('#btn-hide').click(function () {
                $('#container').hide();
                document.getElementById("section").style.width = "100%";
            });

            $('#btn-show').click(function () {
                $('#container').show();
                document.getElementById("section").style.width = "66%";
            });
        });
    </script>
    <script type="text/javascript">
        $("button[name='btn']").attr("disabled", "disabled").button('refresh');
        $("button[name='btn1']").attr("disabled", "disabled").button('refresh');
        $("button[name='btn2']").attr("disabled", "disabled").button('refresh');

        $(document).ready(function () {
            $("input[type='radio']").click(function () {
                $("button[name='btn1']").removeAttr("disabled").button('refresh');


                var radioValue = $("input[name='optradio']:checked").val();
                var radioValue2 = $("input[name='optradio2']:checked").val();
                 if(radioValue2){
                     $("button[name='btn2']").removeAttr("disabled").button('refresh');

                 }
                if (radioValue=='Retrait au magasin') {
                     $('#m3').remove();
                    $('#p2').hide();
                }else if(radioValue=='livraison'){
                    $('#m3').show();
                }
            });

        });


        $('#btn1').click(function () {

            var $optradio= $("input[name='optradio']:checked").val();
            var output1 = document.getElementById('methode');
            output1.innerHTML = $optradio;
        });


        $('#btn2').click(function () {

            var $optradio= $("input[name='optradio2']:checked").val();
            var output1 = document.getElementById('selection');
            output1.innerHTML = $optradio;
        });

        $('#btn').click(function () {

            var input1 = document.getElementById("g").value;
            var input2 = document.getElementById("v").value;
            var input3 = document.getElementById("p").value;
            document.getElementById("gouv").innerHTML = input1;
            document.getElementById("ville").innerHTML = input2;
            document.getElementById("postal").innerHTML = input3;

        });

        $('#select_id').change(function(){
            $("button[name='btn']").removeAttr("disabled").button('refresh');

            var output1 = document.getElementById('adr');
            output1.innerHTML = $(this).val();
        })

    </script>

@endsection

