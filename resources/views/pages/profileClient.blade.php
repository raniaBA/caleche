@extends('master-blade/app')
@section('title','panier')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection
@section('bodyStyle','grad')
@section('content')

    @include("partiels.navbar")
    <style type="text/css">
        #map {
            width: 100%;
            height: 400px;
        }
        #map2 {
            width: 100%;
            height: 400px;
        }
        .mapControls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #searchMapInput {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 50%;
        }

        #searchMapInput:focus {
            border-color: #4d90fe;
        }
    </style>
    i
    <div class=" cont margTop container">
        <h3>Mon profile</h3>
        <hr>
        <div class="row">
            <div style="border: 1px solid lightgray;margin: 1pc; width: 40pc;padding: 1pc;background-color:#d4baba30">
                <div style="margin-top: 1pc;"><strong>Nom:</strong> {{Auth::user()->name}}
                </div>
                <div style="margin-top: 1pc;"><strong>Prénom:</strong> {{Auth::user()->prenom}}
                    <div style="margin-top: 1pc;"><strong>Téléphone:</strong> {{Auth::user()->phone}}</div>
                    <div style="margin-top: 1pc;"><strong>Gouvernorat: </strong> {{Auth::user()->gouvernorat}}
                        <button type="submit" class="btn btn-primery" style="float: right" data-toggle="modal"
                                data-target="#myModal">Modifier
                        </button>
                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <h3 style="margin: 2pc">Vos zone de livraison</h3>

            <div class="col-md-6">
                <div style="border: 1px solid lightgray;padding: 2pc">
                    <ul>
                    @foreach($zones as $z)
                    <li>{{$z->adresse}}</li>
                    @endforeach
                    </ul>
                </div>
            </div>

        </div>
        <h3>Ajouter une adresse de livraison</h3>
        <hr>
        <input id="searchMapInput" class="mapControls" type="text" placeholder="Enter a location">
        <div id="map"></div>
        <form id="form" method="post" action="{{route('storeAdress')}}" >
            @csrf
        <div id="geoData">
            <input type="text" name="lat" id="lat-span" value="" hidden>
            <input type="text" name="lng" id="lon-span" value="" hidden>
            <input type="text" name="adresse" id="location-snap" value="" hidden><br>
            <input type="text" name="msg" id="msg" value="" hidden><br>
        <button class="btn btn-primery">Ajouter</button>
        </div>
        </form>

    </div>



    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="{{url('/profileUpdate')}}">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modifier votre profile</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">nom</label>
                                <input type="text" class="form-control" value="{{Auth::user()->name}}"   name="name" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">prenom</label>
                                <input type="text" class="form-control"  value="{{Auth::user()->prenom}}" name="prenom" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">téléphone</label>
                                <input type="text" class="form-control" name="phone"  value="{{Auth::user()->phone}}"required="" aria-required="true">

                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="form-label">gouvernorat</label>
                                <input type="text" class="form-control"  value="{{Auth::user()->gouvernorat}}" name="gouvernorat" required=""
                                       aria-required="true">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" >Update</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent

<script>
        function initMap() {

            // var map2 = new google.maps.Map(document.getElementById('map2'), {
            //     zoom: 17,
            //     center: {lat: 36.818338, lng: 10.178565},
            //     mapTypeId: 'terrain'
            // });
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: {lat: 36.818338, lng: 10.178565},
                mapTypeId: 'terrain'
            });
            var Coords = [
                    @for($i=0;$i<count($poly); $i=$i+2)
                {
                    lat: parseFloat(['{{$poly[$i]}}']), lng: parseFloat(['{{$poly[$i+1]}}'])
                },
                @endfor
            ];
            console.log('cord', Coords);


            // Construct the polygon.
            var zone = new google.maps.Polygon({
                paths: Coords,
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35
            });
            // Marker.
            var marker = new google.maps.Marker({
                position: {lat: 36.818338, lng: 10.178565},
                animation: google.maps.Animation.DROP,
                map: map,
                label: {text: "La caléche!", color: "white"}
            });
            zone.setMap(map);


            var input = document.getElementById('searchMapInput');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function () {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();

                /* If the place has a geometry, then present it on a map. */
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {

                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }
                marker.setIcon(({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                if (!google.maps.geometry.poly.containsLocation(place.geometry.location, zone)){
                    alert('The address is outside of the area.');
                    document.getElementById('msg').setAttribute('value', '0');
                }else {
                    document.getElementById('msg').setAttribute('value', '1');
                }



                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);

                /* Location details */
                // document.getElementById('location-snap').innerHTML = place.formatted_address;
                // document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
                // document.getElementById('lon-span').innerHTML = place.geometry.location.lng();

                document.getElementById('location-snap').setAttribute('value', place.formatted_address);
                document.getElementById('lat-span').setAttribute('value', place.geometry.location.lat());
                document.getElementById('lon-span').setAttribute('value', place.geometry.location.lng());
            });
        }
    </script>

     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSkRhBbrsIG6XBn8Xt5qGBny6Anh6VXk8&libraries=places&callback=initMap"
            async defer></script>

@endsection