@extends('master-blade/app')
@section('title','detail')
@section('topCss')
    @parent

@endsection

@section('topjs')
    @parent

@endsection

<!-- Navigation-->
@section('content')
    @include("partiels.navbar")
    <!-- Header -->
    <div class="container">

        <div class="margTop row">
            @if($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$errors->first()}}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <div class="col-md-7">

            <!-- Carousel Default -->
                <section class="carousel-default">
                    <div id="carousel-default" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-default" data-slide-to="0" class=""></li>
                            <li data-target="#carousel-default" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-default" data-slide-to="2" class="active"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <!-- NOTE: Bootstrap v4 changes class name to carousel-item -->
                            <div class="item">
                                <img src="{{asset('img/c1.jpg')}}" alt="First slide">
                                <div class="carousel-caption">

                                    <p>She is probably taking a picture</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="{{asset('img/c1.jpg')}}" alt="Second slide">
                                <div class="carousel-caption">

                                    <p>And washed the spider out</p>
                                </div>
                            </div>
                            <div class="item active">
                                <img src="{{asset('img/c1.jpg')}}" alt="Third slide">
                                <div class="carousel-caption">

                                    <p>She is probably taking a picture</p>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-default" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-default" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </section>
                <!-- /end Carousel Default -->

            </div>
            <div class="col-md-5">
                <button type="button" class="btnP btn btn-lg">{{$det->price}}</button>

                <div class="section-title2">
                    <h3>{{$det->name}}</h3>
                </div>
                <strong>Deserts</strong>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sed
                    commodo.</p>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sed
                    commodo.</p>
                <strong>Heure d'ouverture 08:00h-12.00h|15:00h-22.00h</strong>

                <div class="row">
                    <div class="col-md-6">
                        <div class="rate">
                            <input type="radio" id="star5" name="rate" value="5"/>
                            <label for="star5" title="text">5 stars</label>
                            <input type="radio" id="star4" name="rate" value="4"/>
                            <label for="star4" title="text">4 stars</label>
                            <input type="radio" id="star3" name="rate" value="3"/>
                            <label for="star3" title="text">3 stars</label>
                            <input type="radio" id="star2" name="rate" value="2"/>
                            <label for="star2" title="text">2 stars</label>
                            <input type="radio" id="star1" name="rate" value="1"/>
                            <label for="star1" title="text">1 star</label>

                        </div>
                    </div>
                    <p class="margP">576 avis sur la caléche</p>
                </div>
                <a type="submit" class="btnPr btn btn-lg" href="{{ route('shoppingCart',$det->id)}}">Ajouter au
                    panier</a>


            </div>

        </div>

        <div class="margB row">
            <h4><strong>Vous pouvez aussi aimer</strong></h4>


            <!-- carousel -->
            <div class="carousel-size col-md-6 col-md-offset-3">
                <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-md-3 col-sm-6 ">
                                <div class="">

                                    <img src="{{asset('img/specials/1.jpg')}}" class="img-responsive" alt="">
                                    <h5>NOS SALADES</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec
                                        ornare diam sed commodo.</p>

                                    <button type="button" class="btn btn-dark">En savoir plus</button>

                                </div>
                            </div>
                        </div>
                        @foreach($produits as $pr)

                            <div class="item">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="">

                                        <img src="{{asset('img/specials/2.jpg')}}" class="img-responsive" alt="">
                                        <h5>{{$pr->name}}</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo
                                            nec ornare diam sed commodo.</p>
                                        <button type="button" class="btn btn-dark">En savoir plus</button>
                                    </div>
                                </div>
                            </div>
                        @endforeach


                    </div>
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i
                                class="glyphicon glyphicon-chevron-left"></i></a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next"><i
                                class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>

            <!-- carousel -->
        </div>
    </div>

@endsection
<!-- Contact Section -->

@section("footer")
    @parent

@endsection
@section("footerJs")
    @parent

@endsection