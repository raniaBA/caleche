<?php

/*
|----------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//Route::get('/index', 'IndexController@showIndex');
//Route::get('/categorie/{id}', 'CategorieController@showCategorie');
//Route::get('/categorie', 'CategorieController@showCategorie');
//Route::get('/detail', 'DetailController@showDetail');
Route::get('/',['as'=>'index','uses'=>'IndexController@showIndex']);
Route::post('contact',['as'=>'contact','uses'=>'ContactController@PostContact']);
//Route::get('/detail',['as'=>'detail','uses'=>'DetailController@showDetail']);
Route::get('/panier',['as'=>'panier','uses'=>'PanierController@showPanier']);
Route::post('/storeAdress',['as'=>'storeAdress','uses'=>'ProfileClientController@storeAdress']);

Route::post('/shoppingCart/{id}',['as'=>'shoppingCart','uses'=>'PanierController@shopping']);
Route::get('/addQnt/{id}',['as'=>'addQnt','uses'=>'PanierController@addQnt']);
Route::get('/decQnt/{id}',['as'=>'decQnt','uses'=>'PanierController@decQnt']);

Route::get('/deleteCartItem/{id}',['as'=>'deleteCartItem','uses'=>'PanierController@delete']);
Route::get('/categorie/{id}',['as'=>'categorie','uses'=>'CategorieController@showCategorie']);
Route::get('/detail/{id}',['as'=>'detail','uses'=>'DetailController@showDetail']);
Route::get('/profile',['as'=>'profile','uses'=>'ProfileClientController@showProfile']);



Route::get('/paiement',['as'=>'paiement','uses'=>'PaiementController@showpaiement']);
Route::post('/paiementSave',['as'=>'paiementSave','uses'=>'PaiementController@addV']);
Route::post('/profileUpdate' ,'ProfileClientController@update');



Route::get('/polygone',['as'=>'polygone','uses'=>'PolygoneController@showpolygone']);
Route::get('/admin',['as'=>'adminPolygone','uses'=>'AdminController@showAdmin']);
Route::get('/panierTest',['as'=>'panierTest','uses'=>'PanierController@test']);


Route::get('/shoppingCart/{id}',['as'=>'shoppingCart','uses'=>'PanierController@shopping']);
Route::get('/deleteCartItem/{id}',['as'=>'deleteCartItem','uses'=>'PanierController@delete']);
Route::post('/register',['as'=>'register','uses'=>'Auth.RegisterController@create']);




//Route::get('/panier', 'PanierController@showPanier');


//Route::post('/paiementSave', 'PaiementController@adde');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
